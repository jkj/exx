/**
 * Copyright (c) 2016 Voidware Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS," WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 * 
 * contact@voidware.com
 */

#include <windows.h>
#include <stdio.h>

#include "winerror.h"

bool debugMode = false;

#define ENC_SUFFIX      ".e2"
#define AES_SUFFIX      ".aes"

bool exec(char* cmd)
{
    bool res = false;

    PROCESS_INFORMATION procInfo;
    STARTUPINFO startInfo;

    memset(&startInfo, 0, sizeof(startInfo));
    startInfo.cb = sizeof(startInfo);

    memset(&procInfo, 0, sizeof(procInfo));
    
    BOOL v = CreateProcess(NULL,
                           cmd,
                           NULL, // security attributes
                           NULL, // thread attributes
                           FALSE, // handles
                           NORMAL_PRIORITY_CLASS,
                           NULL, // env
                           NULL, // cwd
                           &startInfo,
                           &procInfo);

    if (v)
    {
        WaitForSingleObject(procInfo.hProcess, INFINITE);
        res = true;
    }
    else
    {
        WinError();
    }
    return res;
}

bool exists(const char* name)
{
    DWORD dwAttrib = GetFileAttributes(name);

    return (dwAttrib != INVALID_FILE_ATTRIBUTES && 
            !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

bool copy(const char* src, const char* dst)
{
    bool res = CopyFile(src, dst, FALSE) != 0;
    if (!res)
        WinError();
    return res;
}

bool deleteFile(const char* name)
{
    if (debugMode) printf("deleting file '%s'\n", name);
    bool res = DeleteFile(name) != 0;
    if (!res)
        WinError();
    return res;
}

// -----------------

bool backup(const char* name)
{
    char bak[MAX_PATH];
    strcat(strcpy(bak, name), ".bak");
    if (debugMode) printf("backup, '%s' -> '%s'\n", name, bak);
    return copy(name, bak);
}

bool encAES(const char* name, const char* pw)
{
    char buf[MAX_PATH];
    sprintf(buf, "aescrypt -e -p %s %s", pw, name);
    if (debugMode) printf("%s\n", buf);
    return exec(buf);
}

bool decAES(const char* name, const char* pw)
{
    char buf[MAX_PATH];
    sprintf(buf, "aescrypt -d -p %s %s", pw, name);
    if (debugMode) printf("%s\n", buf);
    return exec(buf);
}

bool encENC(const char* name, const char* pw)
{
    char buf[MAX_PATH];
    sprintf(buf, "enc -e -inplace -p=%s %s", pw, name);
    if (debugMode) printf("%s\n", buf);
    return exec(buf);
}

bool decENC(const char* name, const char* pw)
{
    char buf[MAX_PATH];
    sprintf(buf, "enc -d -p=%s %s", pw, name);
    if (debugMode) printf("%s\n", buf);
    return exec(buf);
}

static bool copyRemoveSuffix(char* dst, const char* src, const char* suf)
{
    // check given suffix is present
    const char* p = strrchr(src, '.');    
    bool res = p && !strcmp(p, suf);
    if (res)
    {
        size_t a = p - src;
        if (dst != src) memcpy(dst, src, a);
        dst[a] = 0;
    }
    return res;
}

bool encodeFile(const char* name, const char* pw)
{
    char buf[MAX_PATH];    
    strcat(strcpy(buf, name), AES_SUFFIX);
    strcat(buf, ENC_SUFFIX);

    bool res = !exists(buf) || backup(buf);
    if (res)
    {
        res = encAES(name, pw);
        if (res)
        {
            strcat(strcpy(buf, name), AES_SUFFIX);
            res = encENC(buf, pw);

            if (res)
            {
                deleteFile(name);
                strcat(strcpy(buf, name), "~");
                if (exists(buf)) deleteFile(buf);
            }
        }
    }
    return res;
}

bool decodeFile(const char* name, const char* pw)
{
    char buf[MAX_PATH];

    // remove both suffix to find final file name
    bool res = copyRemoveSuffix(buf, name, ENC_SUFFIX) &&
        copyRemoveSuffix(buf, buf, AES_SUFFIX);

    if (res)
    {
        res = !exists(buf);
        if (res)
        {
            res = decENC(name, pw);
            if (res)
            {
                copyRemoveSuffix(buf, name, ENC_SUFFIX);
                res = decAES(buf, pw);

                // always delete intermediate
                deleteFile(buf);
            }
        }
        else
            printf("ERROR: decoded file already exists, '%s'\n", buf);
    }
    else
        printf("ERROR: encoded file not correct form '%s'\n", name);

    return res;
}

int main(int argc, char** argv)
{
    int i;
    bool encode = false;
    bool decode = false;
    
    for (i = 1; i < argc; ++i)
    {
        if (argv[i][0] == '-')
        {
            if (!strcmp(argv[i], "-debug")) debugMode = true;
            else if (!strcmp(argv[i], "-e")) encode = true;
            else if (!strcmp(argv[i], "-d")) decode = true;
            else 
            {
                printf("unrecognised option '%s'\n", argv[i]);
                return -1;
            }
        }
    }

    if (encode == decode)
    {
        printf("Usage: %s [-e] [-d] [-debug] [file...]\n", argv[0]);
        return -1;
    }

    char pw[256];
    printf("passcode: ");
    fflush(stdout);
    scanf("%s", pw);

    for (i = 1; i < argc; ++i)
    {
        if (argv[i][0] != '-')
        {
            if (!exists(argv[i]))
            {
                printf("No such file: '%s'\n", argv[i]);
                continue;
            }

            if (encode)
            {
                if (!encodeFile(argv[i], pw))
                {
                    printf("FAILED encode '%s'\n", argv[i]);
                    return -1;
                }
            }
            else
            {
                if (!decodeFile(argv[i], pw))
                {
                    printf("FAILED decode '%s'\n", argv[i]);
                    return -1;
                }
            }
        }
    }
    
    return 0;
}
